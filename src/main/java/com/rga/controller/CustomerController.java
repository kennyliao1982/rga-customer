package com.rga.controller;

import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rga.entity.Customer;
import com.rga.error.AppException;
import com.rga.json.JsonResponse;
import com.rga.json.JsonResponseWithData;
import com.rga.service.CustomerService;

/**
 * controller of the CRUD operation for customer data
 * 
 * @author kenny
 *
 */
@RestController
@RequestMapping(value = "/api")
public class CustomerController {
	private Logger logger = LoggerFactory.getLogger(CustomerController.class);

	@Autowired
	private CustomerService customerService;

	/**
	 * Listing all customers
	 */
	@RequestMapping(value = "/customer", method = RequestMethod.GET)
	public JsonResponseWithData<List<Customer>> getAllCustomers() {
		JsonResponseWithData<List<Customer>> response = new JsonResponseWithData<List<Customer>>();
		try {
			List<Customer> customerList = customerService.getAllCustomers();
			response.setData(customerList);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setMessage(e.getMessage());
			logger.error(ExceptionUtils.getStackTrace(e));
		}
		return response;
	}

	/**
	 * Reading a single customer
	 */
	@RequestMapping(value = "/customer/{customerId}", method = RequestMethod.GET)
	public JsonResponseWithData<Customer> getCustomer(@PathVariable Long customerId) {
		JsonResponseWithData<Customer> response = new JsonResponseWithData<Customer>();
		try {
			Customer customer = customerService.getCustomer(customerId);
			response.setData(customer);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setMessage(e.getMessage());
			logger.error(ExceptionUtils.getStackTrace(e));
		}
		return response;
	}

	/**
	 * Creating a single customer
	 */
	@RequestMapping(value = "/customer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public JsonResponseWithData<Customer> createCustomer(@RequestBody Customer customer) {
		JsonResponseWithData<Customer> response = new JsonResponseWithData<Customer>();
		try {
			customerService.saveCustomer(customer);
			response.setData(customer);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setMessage(e.getMessage());
			logger.error(ExceptionUtils.getStackTrace(e));
		}
		return response;
	}

	/**
	 * Updating a single customer
	 */
	@RequestMapping(value = "/customer/{customerId}", method = RequestMethod.PUT)
	public JsonResponseWithData<Customer> updateCustomer(@PathVariable Long customerId, @RequestBody Customer customer) {
		JsonResponseWithData<Customer> response = new JsonResponseWithData<Customer>();
		try {
			Customer oldCustomer = customerService.getCustomer(customerId);
			if (oldCustomer != null) {
				oldCustomer.setEmail(customer.getEmail());
				oldCustomer.setGender(customer.getGender());
				oldCustomer.setName(customer.getName());
				customerService.saveCustomer(oldCustomer);
				response.setData(oldCustomer);
			}
		} catch (Exception e) {
			response.setSuccess(false);
			response.setMessage(e.getMessage());
			logger.error(ExceptionUtils.getStackTrace(e));
		}
		return response;
	}

	/**
	 * Deleting a single customer
	 */
	@RequestMapping(value = "/customer/{customerId}", method = RequestMethod.DELETE)
	public JsonResponse deleteCustomer(@PathVariable Long customerId) {
		JsonResponse response = new JsonResponse();
		try {
			customerService.deleteCustomer(customerId);
		} catch (AppException e) {
			response.setSuccess(false);
			response.setMessage(e.getMessage());
			logger.error(ExceptionUtils.getStackTrace(e));
		}
		return response;
	}
}
