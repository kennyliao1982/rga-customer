package com.rga.springconfig;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Spring Web MVC related configuration
 * 
 * @author kenny
 *
 */
@Configuration
@EnableWebMvc
@ComponentScan("com.rga")
public class WebMvcConfig extends WebMvcConfigurerAdapter {

}