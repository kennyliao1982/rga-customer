package com.rga.springconfig;

import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;

/**
 * Spring Session initializer
 * 
 * @author kenny
 *
 */
public class HttpSessionAppInitializer extends AbstractHttpSessionApplicationInitializer {

}
