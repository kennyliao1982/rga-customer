package com.rga.springconfig;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.util.StringUtils;

/**
 * Spring Security related configuration
 * 
 * @author kenny
 *
 */
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private Environment env;

	@Autowired
	private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

	@Autowired
	private MySavedRequestAwareAuthenticationSuccessHandler authenticationSuccessHandler;

	@Autowired
	private LogoutFilter logoutFilter;

	/**
	 * main settings of Spring Security
	 * 1.define what url pattern should be protected.
	 * 2.define parameter name of 'username' and 'password'
	 * 3.define customized handlers, please see comments of the inner classes below.
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint).and().authorizeRequests().antMatchers("/api/**").authenticated().and().formLogin()
				.loginProcessingUrl("/j_spring_security_check").usernameParameter("j_username").passwordParameter("j_password").successHandler(authenticationSuccessHandler)
				.failureHandler(new SimpleUrlAuthenticationFailureHandler()).and().addFilter(logoutFilter).logout();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser(env.getRequiredProperty("admin.name")).password(env.getRequiredProperty("admin.password")).roles("ADMIN");
	}

	@Bean
	public MySavedRequestAwareAuthenticationSuccessHandler mySuccessHandler() {
		return new MySavedRequestAwareAuthenticationSuccessHandler();
	}

	@Bean
	public SimpleUrlAuthenticationFailureHandler myFailureHandler() {
		return new SimpleUrlAuthenticationFailureHandler();
	}

	@Bean
	public RestAuthenticationEntryPoint authenticationEntryPoint() {
		return new RestAuthenticationEntryPoint();
	}

	@Bean
	public NoRedirectLogoutSuccessHandler restLogoutFilter() {
		return new NoRedirectLogoutSuccessHandler();
	}

	@Bean
	public LogoutFilter logoutFilter() {
		return new LogoutFilter(new NoRedirectLogoutSuccessHandler(), new SecurityContextLogoutHandler());
	}

	/**
	 * Spring Security's default behavior of processing an unauthorized access
	 * is to redirect the client to the login page. To fullfil the restful
	 * architecture, change it to response a http status 401
	 * 
	 */
	class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {
		@Override
		public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
		}
	}

	/**
	 * Spring Security will redirect client to the original requested url after
	 * the client has successfully logined into the system. To fullfil the
	 * restful architecture, no redirect is desired.
	 * 
	 */
	class MySavedRequestAwareAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

		private RequestCache requestCache = new HttpSessionRequestCache();

		@Override
		public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
			SavedRequest savedRequest = requestCache.getRequest(request, response);

			if (savedRequest == null) {
				clearAuthenticationAttributes(request);
				return;
			}
			String targetUrlParam = getTargetUrlParameter();
			if (isAlwaysUseDefaultTargetUrl() || (targetUrlParam != null && StringUtils.hasText(request.getParameter(targetUrlParam)))) {
				requestCache.removeRequest(request, response);
				clearAuthenticationAttributes(request);
				return;
			}

			clearAuthenticationAttributes(request);
		}

		public void setRequestCache(RequestCache requestCache) {
			this.requestCache = requestCache;
		}
	}

	/**
	 * Spring Security will respond http status 302 after the client do the
	 * logout. To fullfil the restful architecture, just give http status 200.
	 * 
	 */
	class NoRedirectLogoutSuccessHandler implements LogoutSuccessHandler {
		@Override
		public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
			// no redirect ..
		}
	}
}
