package com.rga.springconfig;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Spring Security initializer
 * 
 * @author kenny
 *
 */
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}