package com.rga.service;

import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rga.entity.Customer;
import com.rga.error.AppException;
import com.rga.error.ErrorCode;
import com.rga.repository.CustomerRepository;

/**
 * service of the CRUD operation for customer data
 * 
 * @author kenny
 *
 */
@Service
@Transactional(readOnly = true)
public class CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	@Transactional(rollbackFor = AppException.class)
	public void saveCustomer(Customer customer) throws AppException {
		try {
			customerRepository.save(customer);
		} catch (Exception e) {
			throw new AppException(ErrorCode.RUNTIME_ERROR, ExceptionUtils.getStackTrace(e));
		}
	}

	public Customer getCustomer(Long id) {
		return customerRepository.findOne(id);
	}

	public List<Customer> getAllCustomers() {
		return customerRepository.findAll();
	}

	@Transactional(rollbackFor = AppException.class)
	public void deleteCustomer(Long id) throws AppException {
		try {
			customerRepository.delete(id);
		} catch (Exception e) {
			throw new AppException(ErrorCode.RUNTIME_ERROR, ExceptionUtils.getStackTrace(e));
		}
	}

}
