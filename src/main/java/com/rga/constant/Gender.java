package com.rga.constant;

/**
 * Constant for the customer gender
 * 
 * @author kenny
 * 
 */
public enum Gender {
	MALE, FEMALE
}
