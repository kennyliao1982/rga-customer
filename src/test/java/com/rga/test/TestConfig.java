package com.rga.test;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.rga.repository.CustomerRepository;
import com.rga.service.CustomerService;

@Configuration
public class TestConfig {

	@Bean
	public CustomerService customerServiceMock() {
		return Mockito.mock(CustomerService.class);
	}

	@Bean
	public CustomerRepository customerRepositoryMock() {
		return Mockito.mock(CustomerRepository.class);
	}
}
