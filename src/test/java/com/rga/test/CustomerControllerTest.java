package com.rga.test;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rga.constant.Gender;
import com.rga.controller.CustomerController;
import com.rga.entity.Customer;
import com.rga.service.CustomerService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
@WebAppConfiguration
public class CustomerControllerTest {
	private MockMvc mockMvc;

	@Autowired
	private CustomerService customerServiceMock;
	
	@InjectMocks
	private CustomerController customerController;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(customerController).build();
	}
	
	@Test
	public void testGetSingleCustomer() throws Exception {
		Customer customer = new Customer();
		customer.setEmail("kenny@test.com");
		customer.setGender(Gender.MALE);
		customer.setName("kenny");
		customer.setId(1L);
		
		when(customerServiceMock.getCustomer(1L)).thenReturn(customer);
		
		mockMvc.perform(
				get("/api/customer/1"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data.email").value(customer.getEmail()));
	}
	
	@Test
	public void testGetAllCustomers() throws Exception {
		Customer customer = new Customer();
		customer.setEmail("kenny@test.com");
		customer.setGender(Gender.MALE);
		customer.setName("kenny");
		customer.setId(1L);
		
		Customer customer2 = new Customer();
		customer2.setEmail("alice@test.com");
		customer2.setGender(Gender.FEMALE);
		customer2.setName("alice");
		customer2.setId(2L);
		
		
		when(customerServiceMock.getAllCustomers()).thenReturn(Arrays.asList(customer, customer2));
		
		mockMvc.perform(
				get("/api/customer"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data[1].email").value(customer2.getEmail()));
	}

	@Test
	public void testCreateCustomer() throws Exception {
		Customer customer = new Customer();
		customer.setEmail("kenny@test.com");
		customer.setGender(Gender.MALE);
		customer.setName("kenny");
		
		ObjectMapper om = new ObjectMapper();
		mockMvc.perform(
				post("/api/customer").contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(customer)))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data.name").value(customer.getName()));
	}
	
	@Test
	public void testUpdateCustomer() throws Exception {
		Customer oldCustomer = new Customer();
		oldCustomer.setEmail("kenny@test.com");
		oldCustomer.setGender(Gender.MALE);
		oldCustomer.setName("kenny");
		oldCustomer.setId(1L);
		
		Customer newCustomer = new Customer();
		newCustomer.setEmail("kenny_new@test.com");
		newCustomer.setGender(Gender.MALE);
		newCustomer.setName("kenny_new");
		
		when(customerServiceMock.getCustomer(1L)).thenReturn(oldCustomer);

		ObjectMapper om = new ObjectMapper();
		mockMvc.perform(
				put("/api/customer/1").contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(newCustomer)))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data.email").value(newCustomer.getEmail()))
				.andExpect(jsonPath("$.data.name").value(newCustomer.getName()));
	}
	
	@Test
	public void testDeleteCustomer() throws Exception {
		Customer oldCustomer = new Customer();
		oldCustomer.setEmail("kenny@test.com");
		oldCustomer.setGender(Gender.MALE);
		oldCustomer.setName("kenny");
		oldCustomer.setId(1L);

		mockMvc.perform(
				delete("/api/customer/1"))
				.andDo(print())
				.andExpect(status().isOk());
	}
}
