RGA Customer Module
===================

This module is built with the following technologies:

1. Spring framework.
1. Spring Security -- manage authentication/authorization.
1. Spring Session -- manage user session information.
1. JPA/Hibernate.

and, for the convenience of testing, the following 2 database are embedded within the module:

1. HSQLDB -- store customer data. (memory database mode, all data will get lost after shuting down the server)
1. Redis -- dependency of Spring session


##API Instructions
This module has tested with REST console (Google chrome extension)

1. login:
	
	> POST http://localhost:8080/rga-customer/j_spring_security_check
	>  	
	> 	parameters:
        >
	> 	j_user_name=admin
        >
	> 	j_password=123456

	*the response contains a header named 'x-auth-token', use it to make another request. 

1. get all customers:
	
	> GET http://localhost:8080/rga-customer/api/customer

1. get a single customer:
	
	> GET http://localhost:8080/rga-customer/api/customer/1

1. create customer data:

	> POST http://localhost:8080/rga-customer/api/customer 
	> 
	> 	request body:
        >
	> 	{
	> 		"name":"kenny",
	> 		"email":"kenny@test.com",
	> 		"gender":"MALE"
	> 	}

1. update customer data:

	> PUT http://localhost:8080/rga-customer/api/customer/1 
	> 	
        >
	> 	request body:
        >
	> 	{
	> 		"name":"kenny2",
	> 		"email":"kenny2@test.com",
	> 		"gender":"MALE"
	> 	}

1. delete a customer
	
	> DELETE http://localhost:8080/rga-customer/api/customer/1


1. logout
	
	> GET http://localhost:8080/rga-customer/logout